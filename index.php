<?php

use App\Controllers\Managers;
use App\Controllers\Initializers;
use App\Controllers\JsonControllers;
use App\Controllers\Routing\Router;
use App\Models\Services;
use App\Models\Services\Recyclers;
use App\Models\Waste;
use App\Models\Waste\Recyclable;

require "config/config.php";

// Get Services and waste quantities data
$dataReader = new JsonControllers\DataReader();
$dataReader->setFileContent(APP_ROOT . "json_data/data.json");

$servicesList = $dataReader->readServices();
$wasteQuantities = $dataReader->formatData(
  $dataReader->readWasteValues()
);

// Get Co2 Emissions data
$co2Reader = new JsonControllers\Co2Reader();
$co2Reader->setFileContent(APP_ROOT . "json_data/co2.json");

$wasteCo2Emissions = $co2Reader->formatData(
  $co2Reader->readValues()
);

// Managers and Initializers
$districtsManager = new Managers\DistrictsManager();
$servicesManager = new Managers\ServicesManager();
$servicesManager->setDistricts($districtsManager);

// Initializers will set the initial values of each entities and add them to their manager
$servicesInit = new Initializers\ServicesInitializer($servicesManager);
$wasteInit = new Initializers\WasteInitializer($districtsManager, $wasteCo2Emissions);

// Initialize each service
foreach ($servicesList as $array) {
  switch ($array['type']) {
    case 'incinerateur':
      $service = new Services\Incinerator();
      $servicesInit->initIncinerator($service, $array);
      break;

    case 'composteur':
      $service = new Services\Composter();
      $servicesInit->initComposter($service, $array);
      break;

    case 'recyclagePlastique':
      $service = new Recyclers\PlasticRecycler();
      $servicesInit->initPlasticRecycler($service, $array);
      break;

    case 'recyclagePapier':
      $service = new Recyclers\PaperRecycler();
      $servicesInit->initPaperRecycler($service, $array);
      break;

    case 'recyclageVerre':
      $service = new Recyclers\GlassRecycler();
      $servicesInit->initGlassRecycler($service, $array);
      break;

    case 'recyclageMetaux':
      $service = new Recyclers\MetalRecycler();
      $servicesInit->initMetalRecycler($service, $array);
      break;

    default:
      break;
  }
}

// Initialize each waste type
foreach ($wasteQuantities as $district) {
  foreach ($district as $wasteName => $value) {
    switch ($wasteName) {
      case 'autre':
        $waste = new Waste\OtherWaste();
        $wasteInit->initOtherWaste($waste, $district[$wasteName]);
        break;

      case 'organique':
        $waste = new Waste\OrganicWaste();
        $wasteInit->initOrganicWaste($waste, $district[$wasteName]);
        break;

      case 'plastiques':
        $waste = new Recyclable\PlasticWaste();
        $wasteInit->initPlasticWaste($waste, $district[$wasteName]);
        break;

      case 'papier':
        $waste = new Recyclable\PaperWaste();
        $wasteInit->initPaperWaste($waste, $district[$wasteName]);
        break;

      case 'verre':
        $waste = new Recyclable\GlassWaste();
        $wasteInit->initGlassWaste($waste, $district[$wasteName]);
        break;

      case 'metaux':
        $waste = new Recyclable\MetalWaste();
        $wasteInit->initMetalWaste($waste, $district[$wasteName]);
        break;

      default:
        break;
    }
  }
}

echo '<pre>';                 // Debug
// echo 'WASTE : ' . PHP_EOL;
// print_r($waste);
// exit;

// Print the left part of the homepage : raw data in each manager
ob_start();

echo PHP_EOL;
echo "<h3>DistrictsManager WasteData : </h3>" . PHP_EOL;
print_r($districtsManager->getData());
echo PHP_EOL;

echo '<hr/><br/>';

echo "<h3>ServicesManager Data : </h3>" . PHP_EOL;
print_r($servicesManager->getData());
echo PHP_EOL;

$beforeProcess = ob_get_clean();

// Process all waste
$servicesManager->dispatchWaste($districtsManager);

// Print the right part of the homepage : processed data in each manager
ob_start();

echo PHP_EOL;
echo "<h3>DistrictsManager WasteData : </h3>" . PHP_EOL;
print_r($districtsManager->getData());
echo PHP_EOL;

echo '<hr/><br/>';

echo "<h3>ServicesManager Data : </h3>" . PHP_EOL;
print_r($servicesManager->getData());
echo PHP_EOL;

$afterProcess = ob_get_clean();

// Basic router
// Useless for now, but I plan to add an API later
$router = new Router();
$router->route();
