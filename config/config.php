<?php

define('APP_ROOT', $_SERVER["DOCUMENT_ROOT"] . '/app/');
define('HOST', '/');

define('VIEWS', APP_ROOT . 'views/');
define('JSON_DATA', APP_ROOT . 'json_data/');

define('PUBLIC_DIR', HOST . 'public/');

define('CSS_DIR', PUBLIC_DIR . 'css/');
define('JS_DIR', PUBLIC_DIR . 'js/');

require "autoload.php";
