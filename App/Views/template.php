<!DOCTYPE html>
<html lang="en" class="dark">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="Content-Type" content="text/html" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="<?= CSS_DIR ?>style.min.css">
  <link defer rel="stylesheet" href="<?= CSS_DIR ?>media_queries.min.css">

  <title>WasteManager</title>
</head>

<body class="color-theme-1">
  <div id="sidebar" class="color-theme-2 flex f-center f-column">
    <h2 class="highlighted">WasteManager</h2>
    <nav class="main-nav"></nav>
  </div>

  <div id="page" class="flex f-center">
    <div class="flex f-center f-column">
      <header class="flex f-center color-theme-1 sticky w-100">
        <h2>Before processing data</h2>
      </header>
      <pre><?= $GLOBALS['beforeProcess'] ?></pre>
    </div>

    <div class="flex f-center f-column">
      <header class="flex f-center color-theme-1 sticky w-100">
        <h2>After processing data</h2>
      </header>
      <pre><?= $GLOBALS['afterProcess'] ?></pre>
    </div>
  </div>
</body>

</html>