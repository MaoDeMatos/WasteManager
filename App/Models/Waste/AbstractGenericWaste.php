<?php

namespace App\Models\Waste;

abstract class AbstractGenericWaste
{
  protected $co2Emission = [];
  protected $quantity = [];

  public function getQuantity(): array
  {
    return $this->quantity;
  }

  public function addQuantity(array $quantities)
  {
    if (empty($this->quantity)) {
      $this->quantity = $quantities;
    } else {
      foreach ($quantities as $type => $value) {
        $this->quantity[$type] += $value;
        $this->quantity[$type] = round($this->quantity[$type], 2);
      }
    }
  }

  public function removeQuantity(array $quantities)
  {
    if (!empty($this->quantity)) {
      foreach ($quantities as $type => $value) {
        $this->quantity[$type] -= $value;
        $this->quantity[$type] = round($this->quantity[$type], 2);
      }
    }
  }

  /**
   * Return all of this waste types
   *
   * @return array
   */
  public function getWasteTypes(): array
  {
    $wasteTypes = [];
    foreach ($this->quantity as $key => $value) {
      $wasteTypes[] = $key;
    }
    return $wasteTypes;
  }

  public function getCo2Emission(): array
  {
    return $this->co2Emission;
  }

  public function setCo2Emission(array $co2Emissions)
  {
    if (empty($this->co2Emission)) {
      $this->co2Emission = $co2Emissions;
    }
  }
}
