<?php

namespace App\Models\Services;

use App\Models\Services\Roles\co2EmitterInterface;


abstract class AbstractServices implements co2EmitterInterface
{
  protected int $totalCapacity = 0,
    $actualCapacity = 0,
    $co2Emission = 0;
  protected array $wasteTypes = [];
  protected bool $full = false;

  public function setTotalCapacity(int $totalCapacity)
  {
    $this->totalCapacity = $totalCapacity;
  }

  public function getTotalCapacity(): int
  {
    return $this->totalCapacity;
  }

  public function addActualCapacity(int $actualCapacity)
  {
    $this->actualCapacity += $actualCapacity;
  }

  public function getActualCapacity(): int
  {
    return $this->actualCapacity;
  }

  public function addCo2Emission(int $co2Emission)
  {
    $this->co2Emission += $co2Emission;
  }

  public function getCo2Emission(): int
  {
    return $this->co2Emission;
  }

  public function setWasteTypes(array $wasteTypes)
  {
    foreach ($wasteTypes as $value) {
      $this->wasteTypes[] = $value;
    }
  }

  public function getWasteTypes(): array
  {
    return $this->wasteTypes;
  }

  public function setFull()
  {
    $this->full = true;
  }

  public function isFull(): bool
  {
    return $this->full;
  }
}
