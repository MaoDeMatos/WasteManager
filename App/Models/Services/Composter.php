<?php

namespace App\Models\Services;

class Composter extends AbstractServices
{
  public function calcCo2Emission(array $co2Emission, int $quantity)
  {
    $this->addCo2Emission($co2Emission['compostage'] * $quantity);
  }
}
