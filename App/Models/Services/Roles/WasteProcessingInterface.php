<?php

namespace App\Models\Services\Roles;

use App\Models\Waste\AbstractGenericWaste;

interface WasteProcessingInterface
{
  public function processWaste(AbstractGenericWaste $waste);
}