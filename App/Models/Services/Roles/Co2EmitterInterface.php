<?php

namespace App\Models\Services\Roles;

interface Co2EmitterInterface
{
  public function calcCo2Emission(array $co2Emission, int $quantity);
}