<?php

namespace App\Models\Services;

abstract class AbstractRecyclers extends AbstractServices
{
  public function calcCo2Emission(array $co2Emission, int $quantity)
  {
    $this->addCo2Emission($co2Emission['recyclage'] * $quantity);
  }
}
