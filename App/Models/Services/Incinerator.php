<?php

namespace App\Models\Services;

use App\Models\Services\Roles\BurnerInterface;

class Incinerator extends AbstractServices implements BurnerInterface
{
  public function calcCo2Emission(array $co2Emission, int $quantity)
  {
    $this->addCo2Emission($co2Emission['incineration'] * $quantity);
  }
}
