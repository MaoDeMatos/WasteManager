<?php

namespace App\Controllers\Roles;

interface ManagersInterface
{
  /**
   * Return this manager's data
   *
   * @param boolean $formatted Set if the variable names are formatted
   */
  public function getData(bool $formatted = true): array;
}
