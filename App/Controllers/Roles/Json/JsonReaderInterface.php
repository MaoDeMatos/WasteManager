<?php

namespace App\Controllers\Roles\Json;

interface JsonReaderInterface
{
  /**
   * Extract the data from a JSON file and add it to this object
   *
   * @param string $path Path to the JSON file to use
   * @return void
   */
  public function setFileContent(string $path);
}
