<?php

namespace App\Controllers\Roles\Json;

interface JsonFormatterInterface
{
  /**
   * Return formatted data
   * 
   * To use on this object properties
   *
   * @param array $array $this->$property to format and output
   * @return array
   */
  public function formatData(array $array);
}
