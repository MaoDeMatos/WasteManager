<?php

namespace App\Controllers\Roles;

interface WasteDispatcherInterface
{
  public function dispatchWaste();
}
