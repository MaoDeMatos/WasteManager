<?php

namespace App\Controllers\Roles;

interface InitializerInterface
{
  public function setManager(ManagersInterface $manager);
}
