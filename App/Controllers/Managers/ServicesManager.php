<?php

namespace App\Controllers\Managers;

use App\Models\Waste;
use App\Models\Services\Composter;
use App\Models\Services\AbstractServices;
use App\Models\Services\AbstractRecyclers;
use App\Controllers\Roles\ManagersInterface;
use App\Controllers\Roles\WasteDispatcherInterface;

class ServicesManager implements ManagersInterface, WasteDispatcherInterface
{
  protected int $totalCapacity = 0;
  protected int $actualCapacity = 0;
  protected int $totalCo2Emission = 0;
  protected DistrictsManager $districtsManager;
  protected array $servicesList = [];

  public function addService(AbstractServices $service)
  {
    $this->servicesList[] = $service;
    $this->addTotalCapacity($service);
  }

  public function getServices()
  {
    return $this->servicesList;
  }

  protected function addTotalCapacity(AbstractServices $service)
  {
    $this->totalCapacity += $service->getTotalCapacity();
    $this->totalCapacity = round($this->totalCapacity, 2);
  }

  public function getTotalCapacity()
  {
    return $this->totalCapacity;
  }

  protected function addActualCapacity(AbstractServices $service)
  {
    $this->actualCapacity += $service->getActualCapacity();
    $this->actualCapacity = round($this->actualCapacity, 2);
  }

  public function getActualCapacity()
  {
    return $this->actualCapacity;
  }

  public function addTotalCo2Emission(AbstractServices $service)
  {
    $this->totalCo2Emission += $service->getCo2Emission();
    $this->totalCo2Emission = round($this->totalCo2Emission, 2);
  }

  public function getTotalCo2Emission()
  {
    return $this->totalCo2Emission;
  }

  public function setDistricts(DistrictsManager $districtsManager)
  {
    $this->districtsManager = $districtsManager;
  }

  /**
   * Cycle trough the services and give them the corresponding waste type, starting with recycler
   */
  public function dispatchWaste()
  {
    $services = $this->getServices();
    $recyclers = [];
    $incinerators = [];

    // Separate recyclers from incinerators
    foreach ($services as $service) {
      if (in_array('all', $service->getWasteTypes())) {
        $incinerators[] = $service;
      } else {
        $recyclers[] = $service;
      }
    }

    // Recyclers first
    foreach ($recyclers as $service) {
      // Service is not full
      if (!$service->isFull()) {
        // Select the type of waste corresponding to the service
        $wasteTypes = $service->getWasteTypes();
        $waste = $this->districtsManager->selectWaste($wasteTypes);

        $this->recycleWaste($service, $waste);
      }
    }

    // Incinerators
    foreach ($incinerators as $service) {
      // Service is not full
      if (!$service->isFull()) {
        $wasteQuantities = $this->districtsManager->getData(false);

        foreach ($wasteQuantities as $property => $value) {
          $waste = $wasteQuantities[$property];
          if ($wasteQuantities[$property] instanceof Waste\AbstractGenericWaste) {
            $this->incinerateWaste($service, $waste);
          }
        }
      }
    }

    // Add total values
    foreach ($services as $service) {
      $this->addActualCapacity($service);
      $this->addTotalCo2Emission($service);
    }
  }

  /**
   * Process waste in a recycler
   *
   * @param AbstractRecyclers|Composter $service Recyclers or composters use the same method
   * @param Waste\AbstractGenericWaste $waste Waste object to recycle
   * @return void
   */
  public function recycleWaste(AbstractRecyclers|Composter $service, Waste\AbstractGenericWaste $waste): void
  {
    $wasteTypes = $service->getWasteTypes();
    $wasteQuantities = $waste->getQuantity();
    $matchingTypes = array_intersect(
      $wasteTypes,
      $waste->getWasteTypes()
    );

    $totalCapacity = $service->getTotalCapacity();
    $co2Emissions = $waste->getCo2Emission();

    foreach ($matchingTypes as $type) {
      if (!$service->isFull()) {
        $quantityToRemove = [];
        $actualCapacity = $service->getActualCapacity();
        $quantityToRemove[$type] = 0;

        $capacityLimitExceeded = ($actualCapacity + $wasteQuantities[$type]) > $totalCapacity;

        // Capacity exceeded
        if ($capacityLimitExceeded) {
          $quantityToAdd = $totalCapacity - $actualCapacity;
          $service->setFull();
        } else {
          $quantityToAdd = $wasteQuantities[$type];
        }

        $quantityToAdd = round($quantityToAdd, 2);

        // Add/remove quantitiy or co2 emission
        $quantityToRemove[$type] = $quantityToAdd;
        $service->addActualCapacity($quantityToAdd);
        $waste->removeQuantity($quantityToRemove);
        $this->districtsManager->removeFromTotalQuantity($quantityToRemove);
        $service->calcCo2Emission($co2Emissions[$type], $quantityToAdd);
      }
    }
  }

  /**
   * Process waste in an incinerator
   *
   * Incinerators use a different method to be able to use all waste types
   * 
   * @param AbstractServices $service
   * @param Waste\AbstractGenericWaste $waste
   * @return void
   */
  public function incinerateWaste(AbstractServices $service, Waste\AbstractGenericWaste $waste): void
  {
    $wasteTypes = $waste->getQuantity();

    $totalCapacity = $service->getTotalCapacity();
    $co2Emissions = $waste->getCo2Emission();

    foreach ($wasteTypes as $type => $quantity) {
      if ($quantity) {
        if (!$service->isFull()) {
          $quantityToRemove = [];
          $actualCapacity = $service->getActualCapacity();
          $quantityToRemove[$type] = 0;

          $capacityLimitExceeded = ($actualCapacity + $quantity) > $totalCapacity;

          // Capacity exceeded
          if ($capacityLimitExceeded) {
            $quantityToAdd = $totalCapacity - $actualCapacity;
            $service->setFull();
          } else {
            $quantityToAdd = $quantity;
          }

          $quantityToAdd = round($quantityToAdd, 2);

          // Add/remove quantitiy or co2 emission
          $quantityToRemove[$type] = $quantityToAdd;
          $service->addActualCapacity($quantityToAdd);
          $waste->removeQuantity($quantityToRemove);
          $this->districtsManager->removeFromTotalQuantity($quantityToRemove);
          $service->calcCo2Emission($co2Emissions[$type], $quantityToAdd);
        }
      }
    }
  }

  public function getData(bool $formatted = true): array
  {
    $servicesData = [];
    foreach ($this as $property => $value) {
      if ($property == 'servicesList') {
        $value = [];
        foreach ($this->$property as $index => $service) {
          $newService = [];
          $serviceType = $index . "_" . basename(get_class($service));
          $newService['TotalCapacity'] = $service->getTotalCapacity();
          $newService['ActualCapacity'] = $service->getActualCapacity();
          $newService['Co2Emission'] = $service->getCo2Emission();
          $newService['WasteTypes'] = $service->getWasteTypes();
          $newService['Full'] = $service->isFull() ? "Yes" : "No";
          $value[$serviceType] = $newService;
        }
      } elseif ($this->$property instanceof DistrictsManager) {
        $value = basename(get_class($this->$property)) . ' Object';
      }

      $property = basename($property);
      $servicesData[$property] = $value;
    }
    return $servicesData;
  }
}
