<?php

namespace App\Controllers\Managers;

use App\Controllers\Roles\ManagersInterface;
use App\Models\Waste;
use App\Models\Waste\Recyclable;

class DistrictsManager implements ManagersInterface
{
  protected $totalQuantity = 0;
  protected Waste\OtherWaste $otherWaste;
  protected Waste\OrganicWaste $organicWaste;
  protected Recyclable\PaperWaste $paperWaste;
  protected Recyclable\GlassWaste $glassWaste;
  protected Recyclable\MetalWaste $metalWaste;
  protected Recyclable\PlasticWaste $plasticWaste;

  public function addWaste(Waste\AbstractGenericWaste $waste)
  {
    $wasteType = get_class($waste);

    // Set property if needed
    if (!isset($this->$wasteType)) {
      $this->$wasteType = $waste;
    } else {
      $this->$wasteType->addQuantity($waste->getQuantity());
    }

    $this->addToTotalQuantity($waste->getQuantity());
  }

  public function addToTotalQuantity(array $quantity)
  {
    if (!is_array($quantity)) {
      $this->totalQuantity += $quantity;
    } else {
      foreach ($quantity as $value) {
        $this->totalQuantity += $value;
        $this->totalQuantity = round($this->totalQuantity, 2);
      }
    }
  }

  public function removeFromTotalQuantity(array $quantity)
  {
    foreach ($quantity as $value) {
      $this->totalQuantity -= $value;
      $this->totalQuantity = round($this->totalQuantity, 2);
    }
  }

  /**
   * Select the corresponding type of waste
   *
   * @param array $wasteTypes Waste types to search for
   * @return array|object Object if used by a recycler, array for incinerators
   */
  public function selectWaste(array $wasteTypes): array|object
  {
    // Incinerators
    if (in_array('all', $wasteTypes)) {
      // Return all
      return $this->getData(false);
    } else {
      // Recyclers
      foreach ($this as $property => $waste) {
        if ($this->$property instanceof Waste\AbstractGenericWaste) {
          // Return corresponding waste
          if (array_intersect($wasteTypes, $this->$property->getWasteTypes())) {
            return $this->$property;
          }
        }
      }
    }
  }

  public function getData(bool $formatted = true): array
  {
    $wasteData = [];
    foreach ($this as $property => $value) {
      if ($this->$property instanceof Waste\AbstractGenericWaste) {
        if (!$formatted) {
          $value = $this->$property;
        } else {
          $value = [];
          // $value['Service'] = $this->$property->getServiceType();
          $value['Quantity'] = $this->$property->getQuantity();
          $value['Co2Emission'] = $this->$property->getCo2Emission();
        }
      }

      $property = basename($property);
      $wasteData[$property] = $value;
    }
    return $wasteData;
  }
}
