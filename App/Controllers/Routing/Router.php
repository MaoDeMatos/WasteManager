<?php

namespace App\Controllers\Routing;

use App\Controllers\Routing\Route\Home;

class Router
{
  protected string $req;

  public function __construct()
  {
    $this->setReq();
  }

  public function getReq()
  {
    return $this->req;
  }

  protected function setReq()
  {
    // Check query existence
    if (
      isset($_GET["req"])
      && !empty($_GET["req"])
    ) {
      // Remove trailing slash for the router
      if (substr($_GET["req"], -1) == '/') {
        $_GET["req"] = substr($_GET["req"], 0, -1);
      }
    } else {
      $_GET["req"] = "";
    }
    $this->req = $_GET["req"];
  }

  public function route()
  {
    switch ($this->getReq()) {
        // case 'api':

        //   break;

      default:
        Home::render();
        break;
    }
  }
}
