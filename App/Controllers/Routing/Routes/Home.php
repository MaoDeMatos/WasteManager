<?php

namespace App\Controllers\Routing\Route;

use App\Controllers\Routing\RouteInterface;

class Home implements RouteInterface
{
  static public function render()
  {
    require VIEWS . 'template.php';
  }
}
