<?php

namespace App\Controllers\Routing;

interface RouteInterface
{
  static public function render();
}