<?php

namespace App\Controllers\Initializers;

use App\Models\Services;
use App\Models\Services\Recyclers;
use App\Controllers\Managers\ServicesManager;
use App\Controllers\Roles\InitializerInterface;
use App\Controllers\Roles\ManagersInterface;

/**
 * Services initializer
 * Add base services values and add them to the services manager
 */
class ServicesInitializer implements InitializerInterface
{
  protected ServicesManager $manager;

  public function __construct(ManagersInterface $manager)
  {
    $this->setManager($manager);
  }

  public function setManager(ManagersInterface $manager)
  {
    if ($manager instanceof ServicesManager) {
      $this->manager = $manager;
    }
  }

  public function initIncinerator(Services\Incinerator $service, array $data)
  {
    $totalCapacity = $data['ligneFour'] * $data['capaciteLigne'];
    $service->setTotalCapacity($totalCapacity);

    $wasteType = ['all'];
    $service->setWasteTypes($wasteType);

    $this->manager->addService($service);
  }

  public function initComposter(Services\Composter $service, array $data)
  {
    $totalCapacity = $data['capacite'] * $data['foyers'];
    $service->setTotalCapacity($totalCapacity);

    $wasteType = ['organique'];
    $service->setWasteTypes($wasteType);

    $this->manager->addService($service);
  }

  public function initPlasticRecycler(Recyclers\PlasticRecycler $service, array $data)
  {
    $totalCapacity = $data['capacite'];
    $service->setTotalCapacity($totalCapacity);

    $wasteType = $data['plastiques'];
    $service->setWasteTypes($wasteType);

    $this->manager->addService($service);
  }

  public function initPaperRecycler(Recyclers\PaperRecycler $service, array $data)
  {
    $totalCapacity = $data['capacite'];
    $service->setTotalCapacity($totalCapacity);

    $wasteType = ['papier'];
    $service->setWasteTypes($wasteType);

    $this->manager->addService($service);
  }

  public function initGlassRecycler(Recyclers\GlassRecycler $service, array $data)
  {
    $totalCapacity = $data['capacite'];
    $service->setTotalCapacity($totalCapacity);

    $wasteType = ['verre'];
    $service->setWasteTypes($wasteType);

    $this->manager->addService($service);
  }

  public function initMetalRecycler(Recyclers\MetalRecycler $service, array $data)
  {
    $totalCapacity = $data['capacite'];
    $service->setTotalCapacity($totalCapacity);

    $wasteType = ['metaux'];
    $service->setWasteTypes($wasteType);

    $this->manager->addService($service);
  }
}
