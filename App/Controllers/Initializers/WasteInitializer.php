<?php

namespace App\Controllers\Initializers;

use App\Models\Waste;
use App\Models\Waste\Recyclable;
use App\Controllers\Managers\DistrictsManager;
use App\Controllers\Roles\InitializerInterface;
use App\Controllers\Roles\ManagersInterface;

/**
 * Waste initializer
 * Add base waste values and add them to the district manager
 */
class WasteInitializer implements InitializerInterface
{
  protected DistrictsManager $manager;
  protected array $co2Emissions = [];

  public function __construct(ManagersInterface $manager, array $co2Emissions)
  {
    $this->setManager($manager);
    $this->setCo2Emissions($co2Emissions);
  }

  public function setManager(ManagersInterface $manager)
  {
    if ($manager instanceof DistrictsManager) {
      $this->manager = $manager;
    }
  }

  public function setCo2Emissions(array $co2Emissions)
  {
    $this->co2Emissions = $co2Emissions;
  }

  public function initOtherWaste(Waste\OtherWaste $waste, array $quantities)
  {
    $waste->setCo2Emission($this->co2Emissions['autre']);
    $waste->addQuantity($quantities);

    $this->manager->addWaste($waste);
  }

  public function initOrganicWaste(Waste\OrganicWaste $waste, array $quantities)
  {
    $waste->setCo2Emission($this->co2Emissions['organique']);
    $waste->addQuantity($quantities);

    $this->manager->addWaste($waste);
  }

  public function initPaperWaste(Recyclable\PaperWaste $waste, array $quantities)
  {
    $waste->setCo2Emission($this->co2Emissions['papier']);
    $waste->addQuantity($quantities);

    $this->manager->addWaste($waste);
  }

  public function initGlassWaste(Recyclable\GlassWaste $waste, array $quantities)
  {
    $waste->setCo2Emission($this->co2Emissions['verre']);
    $waste->addQuantity($quantities);

    $this->manager->addWaste($waste);
  }

  public function initMetalWaste(Recyclable\MetalWaste $waste, array $quantities)
  {
    $waste->setCo2Emission($this->co2Emissions['metaux']);
    $waste->addQuantity($quantities);

    $this->manager->addWaste($waste);
  }

  public function initPlasticWaste(Recyclable\PlasticWaste $waste, array $quantities)
  {
    $waste->setCo2Emission($this->co2Emissions['plastiques']);
    $waste->addQuantity($quantities);

    $this->manager->addWaste($waste);
  }
}
