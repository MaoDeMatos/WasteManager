<?php

namespace App\Controllers\JsonControllers;

use App\Controllers\Roles\Json\JsonReaderInterface;
use App\Controllers\Roles\Json\JsonFormatterInterface;

class Co2Reader
extends AbstractJsonReader
implements
  JsonReaderInterface,
  JsonFormatterInterface
{
  public function readValues()
  {
    return $this->fileContent;
  }

  public function setFileContent(string $path)
  {
    $this->fileContent = json_decode(
      file_get_contents($path),
      true
    );
  }

  public function formatData(array $array)
  {
    foreach ($array as $wasteName => $wasteCo2Array) {
      if (isset($wasteCo2Array['incineration'])) {
        $array[$wasteName] = array($wasteName => $wasteCo2Array);
      }
    }
    return $array;
  }
}
