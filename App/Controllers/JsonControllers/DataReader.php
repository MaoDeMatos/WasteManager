<?php

namespace App\Controllers\JsonControllers;

use App\Controllers\Roles\Json\JsonReaderInterface;
use App\Controllers\Roles\Json\JsonFormatterInterface;

class DataReader
extends AbstractJsonReader
implements
  JsonReaderInterface,
  JsonFormatterInterface
{
  protected array
    $services,
    $wasteValues;

  public function readServices()
  {
    return $this->services;
  }

  public function readWasteValues()
  {
    return $this->wasteValues;
  }

  public function setFileContent(string $path)
  {
    $this->fileContent = json_decode(
      file_get_contents($path),
      true
    );
    $this->services = $this->fileContent['services'];
    $this->wasteValues = $this->fileContent['quartiers'];
  }

  public function formatData(array $array): array
  {
    foreach ($array as $index => $district) {
      foreach ($district as $wasteName => $quantity) {
        if (!is_array($quantity)) {
          $district[$wasteName] = array($wasteName => $quantity);
        }
        $array[$index] = $district;
      }
    }
    return $array;
  }
}
