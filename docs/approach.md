# General approach

My original approach, in terms of planning and organization for this project.

## Planned organization :

Planning/reflexion phase<br />
Split the project into several parts, categorized by their functionnalities (eg.: Setup the project's file tree, decode input JSON file, etc...)<br />
Schedule : Estimate tasks order and duration<br />
Execution : Realize prepared tasks

## Tasks :

1. Prepare what functionalities the app will have
2. Make a class diagram for each part + 1 global to connect them
3. Setup the project's base file tree
4. Add basic JSON decoder to extract data from input .json files
5. Add the base (abstract) classes/interfaces
6. Add services types classes
7. Add waste types classes
8. Complete services methods implementations - with maths
9. Complete waste methods implementations - with maths
10. Complete JSON decoder implementation
11. Add app JSON encoder for output

## Planning :

*I took into account the fact that i'm still waiting for a response from a company*

* 02/10 - Planning and preparation phases finished - Add base classes/interfaces
* 04/10 - Start implementation of services/waste classes - Start JSON decoder
* 08/10 - Previous tasks finished - Start working on the output
