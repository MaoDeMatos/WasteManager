# Progress detail

In this file you will find the timeline of the tasks listed in the [approach.md](approach.md) file, with their starting and ending date each.

I will also update the diagrams.

### 28/09 to 30/09

Task 1 & 2, with no particular order as they are complementary.
Task 3.
Task 4 (start JSON decoder).<br />
I waited the end of [our course about OOP](https://gitlab.com/simplonclermont-p7-cda/td-oop-project-management) to avoid messing the interfaces and the general organization.

### 30/09 to 01/10

Task 5, started 6 (start services classes), but did 7 (start waste classes) instead to have a better view of what to put in services classes.

### 04/10

Task 6/7 progressed, but I changed some things :<br />
I added an entity initializer, to control the creation of the services and to add them to the manager.

### 05/10

Added abstract classes to manage plastic waste & services more easily.<br />
Entities manager updated.<br />
Changed JsonControllers names to better suit the file tree when the encoder will be added.

### 06/10

Added DistrictsManager class to keep track of the waste quantities of each types.<br />
Added waste types.<br />
Updated class diagrams.

### 07/10

Added interfaces for each service type to ease adding more services later on.<br />
Changed waste classes (moslty plastic) to better suit the services.

### 08/10

Changed the functioning of controllers :<br />
Added waste manager and initializer.<br />
Added interfaces to managers, initializers and services.

### 11/10

Changed services Interfaces.<br />
Added properties and methods to waste.<br />
Added processing methods to services.

### 12/10

Changed JSON interfaces.<br />
Changed JSON decoders' way of formatting data.<br />
Started changes on properties types to simplify the usage of methods across all objects.

### 13/10

Visual changes and code cleaning (some variables renamed, some classes reorganized)

### 15/10

Visual corrections and update README.md.<br/>
Added business logic for quantities calculations.<br/>
Update different classes to better suit the recent properties types changes.

### 16/10

Added Co2 calculations and corrections on quantities calculations.<br/>
Adjusted comments and .md files updated.
