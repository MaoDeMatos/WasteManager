# Introduction

This app is a studying project about OOP/SOLID principles.

It is managing waste quantities and Co2 emissions for different services types (incinerators, plastic recyclers...) and for different waste types (organic, paper...).

This app processes data passed by two JSON files :
* Co2 emission values for each waste type
* All waste quantities and services types and maximum capacities

Here you will find :
* The base file tree of the project
* The functionnalities of the app
* The class diagrams of the app

My original approach for this project : [approach.md](docs/approach.md)<br />
To see my progress timeline on the project : [progress.md](docs/progress.md)

Simply clone this repo and start a PHP server on index.php and the app will display the results before AND after processing data !

## Difficulties

*I ended up reworking multiple times the interactions between classes, that's why I didn't respect my original planning but I think I could have done it in time with my original approach*

The major difficulties I encountered were about separating plastic types from other waste.<br/>
I did structural changes and properties changes multiple times before finding a structure that could manage all waste types the same way.<br/>
<br/>
I think I did not use enough interfaces but as I used initializers to prepare objects and managers to do their calculations, my models do not really do anything, except getting/setting/adding and removing values.<br/>
<br/>
As the services/wastes lists are arrays and I used a foreach loop each time, as long as the data structure is the same, we can add new wastes or services easily.

## File Tree :

Base file tree.

```
WasteManager/
  App/
    Controllers/
    json_data/
    Models/
    (Src/ - Frontend
      scss
    Views/)
  config/
    autoload.php - Class autoloader
    config.php - Constants definitions
  docs/
    approach.md - Planning and organization for the project
    progress.md - Timeline of the tasks, ordered by date
    Images for documentation (eg.: class diagrams)
  (public/)
  README.md
```

## Functionalities :

Controllers :
* JsonReader -> read the data from the JSON file
* EntitiesInitializers -> Initialize entities properly and add them to their respective manager
* ServicesManager -> Globally manage the services, keeps track of the total capacity, remaining capacity and total Co2 emission
* DistrictsManager -> Globally manage waste operations, waste total quantity and quantity for each type

The output will simply be displaying the results before AND after processing data :
* Total data of all services (used/free)
* Data of each service
* Total waste data
* Data of each waste type

## Class diagrams :

Lastest class diagrams.

### Global class diagram :

*Sorry if I have used the wrong arrow types*

![](docs/global-cd.png)

### Services :

![](docs/services-cd.png)

### Waste :

![](docs/waste-cd.png)
